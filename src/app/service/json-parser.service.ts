import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DailyAsteroidResponse } from '../model/DailyAsteroidResponse';
import { NearEarthObject } from '../model/NearEarthObject';
import { formatDate } from '@angular/common';
import { TotalAsteroidResponse } from '../model/TotalAsteroidResponse';
import { Observable } from 'rxjs';
import { AllAsteroidResponse } from '../model/AllAsteroidResponse';



@Injectable({
  providedIn: 'root'
})
export class JSONParserService {

  constructor(private _httpClient: HttpClient) {
  }



  //GET DAILY DATA FROM JSON//
  //Allows to get asteroid data parse from URL
  getDailyData(dateToReach: string) : Observable<DailyAsteroidResponse>{

    //1) Build url from date
    let dailyAsteroidURL = "https://www.neowsapp.com/rest/v1/feed?start_date="
        .concat(dateToReach, "&end_date=", dateToReach, "&detailed=true");

    //console.log("URL : " + dailyAsteroidURL);

    //2) Return JSON data from API
    return this._httpClient.get<DailyAsteroidResponse>(dailyAsteroidURL);
  }


  
  //GET TOTAL DATA//
  //Allows to fill totalResponse struct with asteroid data parse from URL
  getTotalData() : Observable<TotalAsteroidResponse>{

    //1) Set URL
    let totalAsteroidURL: string = "http://www.neowsapp.com/rest/v1/stats";

    //2) Return JSON data from API
    return this._httpClient.get<TotalAsteroidResponse>(totalAsteroidURL);
  }



  //GET SEARCH ASTEROID//
  //Allows to fill searchResponse struct with asteroid data parse from URL
  getAllData(nbAsteroidPerPage: number, numPage: number) : Observable<AllAsteroidResponse>{

    //1) Set URL
    let allAsteroidURL: string = "http://www.neowsapp.com/rest/v1/neo/browse?page=" + numPage + "&size=" + nbAsteroidPerPage;

    //2) Return JSON data from API
    return this._httpClient.get<AllAsteroidResponse>(allAsteroidURL);
  }
}
