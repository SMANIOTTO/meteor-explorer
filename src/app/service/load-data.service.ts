import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NearEarthObject } from '../model/NearEarthObject';



@Injectable({
  providedIn: 'root'
})
export class LoadDataService {
  public asteroidSelected: BehaviorSubject<NearEarthObject> = new BehaviorSubject<NearEarthObject>(null);
}
