import { Component, OnInit, Input } from '@angular/core';
import { NearEarthObject } from '../model/NearEarthObject';



@Component({
  selector: 'astExp-asteroid-all-display',
  template: `
    <div *ngIf="asteroid != null">

      <!-- ORBIT PICTURE -->
      <img *ngIf='asteroid.orbital_data.orbit_class.orbit_class_type == "APO"' style="width:60%; margin-top:-50%;" src="../assets/img/apollos_orbit.png">
      <img *ngIf='asteroid.orbital_data.orbit_class.orbit_class_type == "AMO"' style="width:60%; margin-top:-50%;" src="../assets/img/amors_orbit.png">
      <img *ngIf='asteroid.orbital_data.orbit_class.orbit_class_type == "ATE"' style="width:60%; margin-top:-50%;" src="../assets/img/atens_orbit.png">
      <img *ngIf='asteroid.orbital_data.orbit_class.orbit_class_type == "ATI"' style="width:60%; margin-top:-50%;" src="../assets/img/atiras_orbit.png">
      <h3 *ngIf="asteroid.orbital_data == null || asteroid.orbital_data.orbit_class == null">No orbit data found</h3>
      <br>

      <!-- ASTEROID NAME -->
      <h5 *ngIf="asteroid.name != null; else noNameFound">
        {{ asteroid.name }}
      </h5>
      <ng-template #noNameFound>
        <h5>No name found</h5>
      </ng-template>
      <br>

      <!-- ORBIT NAME -->
      <p *ngIf="asteroid.orbital_data != null && asteroid.orbital_data.orbit_class != null; else noOrbitTypeFound">
        <t *ngIf='asteroid.orbital_data.orbit_class.orbit_class_type == "APO"'>Apollos</t>
        <t *ngIf='asteroid.orbital_data.orbit_class.orbit_class_type == "AMO"'>Amors</t>
        <t *ngIf='asteroid.orbital_data.orbit_class.orbit_class_type == "ATE"'>Atens</t>
        <t *ngIf='asteroid.orbital_data.orbit_class.orbit_class_type == "ATI"'>Atiras</t>
      </p>
      <ng-template #noOrbitTypeFound>
        <p>
          Orbit not found
        </p>
        </ng-template>
      <br>

      <!-- TIME -->
      <p *ngIf="asteroid.close_approach_data[0] != null && asteroid.close_approach_data[0].close_approach_date_full != null; else noTimeFound">
        {{ asteroid.close_approach_data[0].close_approach_date_full.substr(12, 16) }}
      </p>
      <ng-template #noTimeFound>
        <p>
          No time found
        </p>
      </ng-template>
      <br>

      <!-- DATE -->
      <p *ngIf="asteroid.close_approach_data[0] != null && asteroid.close_approach_data[0].close_approach_date_full != null; else noDateFound">
        {{ asteroid.close_approach_data[0].close_approach_date_full.substr(0, 10) }}
      </p>
      <ng-template #noDateFound>
        <p>
          No date found
        </p>
      </ng-template>
    </div>
  `,
  styles: [
  ]
})
export class AsteroidAllDisplayComponent implements OnInit {
  @Input() asteroid: NearEarthObject;

  constructor() { }

  ngOnInit(): void {
  }
}
