import { Component, OnInit } from '@angular/core';
import { JSONParserService } from '../service/json-parser.service';
import { DailyAsteroidResponse } from '../model/DailyAsteroidResponse';
import { formatDate } from '@angular/common';
import { NearEarthObject } from '../model/NearEarthObject';
import { Observable } from 'rxjs';
import { AsteroidDetailsComponent } from './asteroid-details.component';
import { LoadDataService } from '../service/load-data.service';



@Component({
  selector: 'astExp-asteroid-daily',
  template: `
    <!-- ASTEROID NB -->
    <div *ngIf="asteroidDailyResponse != null">
      <h3 *ngIf="asteroidDailyResponse.element_count > 0">{{ asteroidDailyResponse.element_count }} asteroid found for the {{ dateToDisplay }}</h3>
      <h3 *ngIf="asteroidDailyResponse.element_count == 0"> No asteroid records for the {{ dateToDisplay }}</h3>

      <!-- ASTEROID LIST -->
      <ul class="list_empty">
        <li class="list_point_hide" *ngFor="let currentAsteroid of asteroidDailyResponse.near_earth_objects[dateToDisplay]">
            <i class="icofont-rounded-right"></i>
            <a href="#" style="color: white;!important">
              <span [class.selected]="selected" (click)="selectAsteroid(currentAsteroid)">
              {{ currentAsteroid.close_approach_data[0].close_approach_date_full.substr(12, 16)}} -
              {{ currentAsteroid.name.replace('(', '').replace(')', '') }}
          </span>
            </a>
        </li>
      </ul>

      <!-- CONTROLS -->
      <div class="col-lg-6 text-center">
        <a href="#" (click)="navigateToDay(-1)">
          <i class="icofont-arrow-left"></i>
        </a>
        <a href="#" (click)="navigateToDay(0)" style="color: white;">
          Today
        </a>
        <a href="#" (click)="navigateToDay(1)">
          <i class="icofont-arrow-right"></i>
        </a>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class AsteroidDailyComponent implements OnInit {
    asteroidSelected:       NearEarthObject;
    dateOfResearch:         Date;
    dateToDisplay:          string;
    asteroidDailyResponse:  DailyAsteroidResponse;  //Asteroid data struct parse from JSON




  constructor(private jsonParser: JSONParserService, private loadDataService: LoadDataService ) 
  {
    this.dateOfResearch = new Date();
    this.dateToDisplay  = formatDate(this.dateOfResearch, 'yyyy-MM-dd', 'en');
  }



  ngOnInit(): void 
  {
    //1) Load today asteroid data
    this.jsonParser.getDailyData(this.dateToDisplay).subscribe(
        (data: DailyAsteroidResponse) => {
          console.log("Data from " + this.dateToDisplay + " get");
          this.loadDailyData(data);
        },

        (err) => { //ERROR getting data
          console.log("Error : " + JSON.stringify(err));
        }
    );
  }
  


  //SELECT asteroid//
  //Allows to select an asteroid in list
  selectAsteroid(currentAsteroid: NearEarthObject){
    this.asteroidSelected = this.asteroidSelected === currentAsteroid ? this.asteroidSelected : currentAsteroid;

    this.loadDataService.asteroidSelected.next(this.asteroidSelected);
    //this.asteroidDetailsComponent.setSelectedAsteroid(this.selectedAsteroid);
  }



  //NAVIGATE TO DAY//
  //Allows to get data from day clicked by button
  navigateToDay(dayToReach: number){

    //1) Get day according to the button click
    if(dayToReach == 0){    //TODAY
        this.dateOfResearch = new Date();
    }
    else{                   //PREVOUS/NEXT day
        this.dateOfResearch.setDate(this.dateOfResearch.getDate()+dayToReach);
    }

    //2) Build URL
    this.dateToDisplay = formatDate(this.dateOfResearch, 'yyyy-MM-dd', 'en');

    //3) Load & Get DATA
    this.getDailyData(this.dateToDisplay);
  }



  //GET DAILY DATA//
  //Alows to call the JSON parser to get data from date
  getDailyData(dateToReach: string){

    //Try to load data from JSON URL
    this.jsonParser.getDailyData(dateToReach).subscribe(

        //Load data into struct
        (data: DailyAsteroidResponse) => {
          this.loadDailyData(data);
          console.log("Data from " + dateToReach + " get");
        },

        //ERROR getting data
        (err) => { 
          console.log("Error : " + JSON.stringify(err));
        }
    );
  }



  //LOAD DAILY DATA//
  //Allows to load data from observable result for avoiding displaying before data loaded.
  loadDailyData(dailyDataGet: DailyAsteroidResponse){
    this.asteroidDailyResponse = dailyDataGet;
  }
}
