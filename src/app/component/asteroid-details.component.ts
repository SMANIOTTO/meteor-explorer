import { Component, OnInit } from '@angular/core';
import { NearEarthObject } from '../model/NearEarthObject';
import { LoadDataService } from '../service/load-data.service';

@Component({
  selector: 'astExp-asteroid-details',
  template: `
    <div class="counts container">

        <div class="row">

          <!-- IDENTITY -->
          <div class="col-lg-3 col-md-6">
            <div class="count-box">
              <i class="icofont-satellite"></i>
              <span data-toggle="counter-up">Identity</span>
              <p *ngIf="asteroidSelected != null">
                <b>Name</b> : {{ asteroidSelected.name.replace('(', '').replace(')', '') }}
                <br>
                <b>Brightness</b> : {{ asteroidSelected.absolute_magnitude_h }}
                <br>
                <b>ID</b> : {{ asteroidSelected.neo_reference_id }}
              </p>
              <p *ngIf="asteroidSelected == null">
                Please, choose an asteroid
              </p>
            </div>
          </div>

          <!-- DISTANCE -->
          <div class="col-lg-3 col-lg-6 mt-5 mt-lg-0">
            <div class="count-box">
              <i class="icofont-map-pins"></i>
              <span data-toggle="counter-up">Distance</span>
              <p *ngIf="asteroidSelected != null">
                <t *ngFor="let current_approach_data of asteroidSelected.close_approach_data">
                  <b>Closest date</b> :
                  {{ current_approach_data.close_approach_date_full.substring(
                    0,
                    current_approach_data.close_approach_date_full.length - current_approach_data.close_approach_date_full.split(' ')[1].length)
                  }}
                  <br>
                  <b>Closest time</b> :
                  {{ current_approach_data.close_approach_date_full.substring(
                    current_approach_data.close_approach_date_full.length - current_approach_data.close_approach_date_full.split(' ')[1].length,
                    current_approach_data.close_approach_date_full.length)
                  }}
                  <br>
                  <b>Distance (astronomical)</b> : ~{{ current_approach_data.miss_distance.astronomical.substring(
                    0, current_approach_data.miss_distance.astronomical.split('.')[0]+7) }} <b>au</b>
                  <br>
                  <b>Distance (kilometers)</b> : ~{{ current_approach_data.miss_distance.kilometers.substring(
                    0, current_approach_data.miss_distance.kilometers.split('.')[0].length) }} <b>km</b>
                  <br>
                  <b>Distance (lunar)</b> : ~{{ current_approach_data.miss_distance.lunar.substring(
                    0, current_approach_data.miss_distance.lunar.split('.')[0].length) }} <b>al</b>
                  <br>
                  <b>Distance (miles)</b> : ~{{ current_approach_data.miss_distance.miles.substring(
                    0, current_approach_data.miss_distance.miles.split('.')[0].length) }} <b>miles</b>
                  <br>
                  <b>Velocity</b> : ~{{ current_approach_data.relative_velocity.kilometers_per_hour.substring(
                    0, current_approach_data.relative_velocity.kilometers_per_hour.split('.')[0].length) }} <b>km/h</b>
                </t>
              </p>
              <p *ngIf="asteroidSelected == null">
                Please, choose an asteroid
              </p>
            </div>
          </div>

          <!-- DANGER -->
          <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
            <div class="count-box">
              <i class="icofont-exclamation-circle"></i>
              <span data-toggle="counter-up">Danger</span>
              <p *ngIf="asteroidSelected != null">
                <b>Dangerous</b> : 
                <t *ngIf="asteroidSelected.is_potentially_hazardous_asteroid"> Yes </t>
                <t *ngIf="!asteroidSelected.is_potentially_hazardous_asteroid"> No </t>
                <br>
                <b>Impact risk</b> :
                <t *ngIf="asteroidSelected.is_sentry_object"> Yes </t>
                <t *ngIf="!asteroidSelected.is_sentry_object"> No </t>
              </p>
              <p *ngIf="asteroidSelected == null">
                Please, choose an asteroid
              </p>
            </div>
          </div>

        </div>

        <br>
        <br>
        <br>

        <div class="row">

          <!-- SIZE -->
          <div class="col-lg-3 col-lg-6 mt-5 mt-md-0">
            <div class="count-box">
              <i class="icofont-crop"></i>
              <span data-toggle="counter-up">Size</span>
              <p *ngIf="asteroidSelected != null">
                <b>Size estimated (kilometers)</b> : ~{{ ((asteroidSelected.estimated_diameter.kilometers.estimated_diameter_max +
                  asteroidSelected.estimated_diameter.kilometers.estimated_diameter_min)/2).toString().substring(0, 7) }} <b>km</b>
                <br>
                <b>Size estimated (feet)</b> : ~{{ ((asteroidSelected.estimated_diameter.feet.estimated_diameter_max +
                  asteroidSelected.estimated_diameter.feet.estimated_diameter_min)/2).toString().substring(0, 7) }} <b>feet</b>
                <br>
                <b>Size estimated (miles)</b> : ~{{ ((asteroidSelected.estimated_diameter.miles.estimated_diameter_max +
                  asteroidSelected.estimated_diameter.miles.estimated_diameter_min)/2).toString().substring(0, 7) }} <b>miles</b>
              </p>
              <p *ngIf="asteroidSelected == null">
                Please, choose an asteroid
              </p>
            </div>
          </div>

          <!-- ORBIT -->
          <div class="col-lg-3 col-lg-6 mt-5 mt-md-0">
            <div class="count-box">
              <i class="icofont-space"></i>
              <span data-toggle="counter-up">Orbit</span>
              <p *ngIf="asteroidSelected != null">
                <b>Orbiting body</b> : {{ asteroidSelected.close_approach_data[0].orbiting_body }}
                <br>
                <b>Orbit similarity</b> : {{ asteroidSelected.orbital_data.orbit_class.orbit_class_description }}
                <br>
                <b>Orbit range</b> : {{ asteroidSelected.orbital_data.orbit_class.orbit_class_range }}
                <br>
                <b>Orbit type</b> : {{ asteroidSelected.orbital_data.orbit_class.orbit_class_type }}
              </p>
              <p *ngIf="asteroidSelected == null">
                Please, choose an asteroid
              </p>
            </div>
          </div>

        </div>

        <br>
        <br>
        <br>

        <div class="row">

          <!-- PARAMETERS -->
            <div class="count-box">
              <i class="icofont-space"></i>
              <span data-toggle="counter-up">Parameters</span>
              <p *ngIf="asteroidSelected != null">
                <b>Aphelion distance</b> : {{ asteroidSelected.orbital_data.aphelion_distance.substring(
                  0, asteroidSelected.orbital_data.aphelion_distance.split('.')[0].length+5) }} <b>au</b>
                <br>
                <b>Ascending node longitude</b> : {{ asteroidSelected.orbital_data.ascending_node_longitude.substring(
                  0, asteroidSelected.orbital_data.ascending_node_longitude.split('.')[0].length+5) }} <b>&omega;</b>
                <br>
                <b>Data arc</b> : {{ asteroidSelected.orbital_data.data_arc_in_days }} <b>day</b>
                <br>
                <b>Eccentricity</b> : {{ asteroidSelected.orbital_data.eccentricity.substring(
                  0, asteroidSelected.orbital_data.eccentricity.split('.')[0].length+5) }}
                <br>
                <b>Epoch osculation</b> : {{ asteroidSelected.orbital_data.epoch_osculation }} <b>TT</b>
                <br>
                <b>Equinox</b> : {{ asteroidSelected.orbital_data.equinox }}
                <br>
                <b>First observation date</b> : {{ asteroidSelected.orbital_data.first_observation_date }}
                <br>
                <b>Inclination</b> : {{ asteroidSelected.orbital_data.inclination.substring(
                  0, asteroidSelected.orbital_data.inclination.split('.')[0].length+5) }} <b>rad</b>
                <br>
                <b>Jupiter tisserand invariant</b> : {{ asteroidSelected.orbital_data.jupiter_tisserand_invariant }} <b>Tp</b>
                <br>
                <b>Last observation date</b> : {{ asteroidSelected.orbital_data.last_observation_date }}
                <br>
                <b>Mean anomaly</b> : {{ asteroidSelected.orbital_data.mean_anomaly.substring(
                  0, asteroidSelected.orbital_data.mean_anomaly.split('.')[0].length+5) }} <b>rad</b>
                <br>
                <b>Mean motion</b> : {{ asteroidSelected.orbital_data.mean_motion.substring(
                  0, asteroidSelected.orbital_data.mean_motion.split('.')[0].length+5) }} <b>rad/s</b>
                <br>
                <b>Minimum orbit intersection</b> : {{ asteroidSelected.orbital_data.minimum_orbit_intersection.substring(
                  0, asteroidSelected.orbital_data.minimum_orbit_intersection.split('.')[0].length+5) }}
                <br>
                <b>Observations used</b> : {{ asteroidSelected.orbital_data.observations_used }}
                <br>
                <b>Orbit determination date</b> : {{ asteroidSelected.orbital_data.orbit_determination_date }}
                <br>
                <b>Orbit id</b> : {{ asteroidSelected.orbital_data.orbit_id }}
                <br>
                <b>Orbit uncertainty</b> : {{ asteroidSelected.orbital_data.orbit_uncertainty }}
                <br>
                <b>Orbital period</b> : {{ asteroidSelected.orbital_data.orbital_period.substring(
                  0, asteroidSelected.orbital_data.orbital_period.split('.')[0].length+2) }} <b>day</b>
                <br>
                <b>Perihelion argument</b> : {{ asteroidSelected.orbital_data.perihelion_argument.substring(
                  0, asteroidSelected.orbital_data.perihelion_argument.split('.')[0].length+5) }} <b>rad</b>
                <br>
                <b>Perihelion distance</b> : {{ asteroidSelected.orbital_data.perihelion_distance.substring(
                  0, asteroidSelected.orbital_data.perihelion_distance.split('.')[0].length+2) }} <b>au</b>
                <br>
                <b>Perihelion time</b> : {{ asteroidSelected.orbital_data.perihelion_time.substring(
                  0, asteroidSelected.orbital_data.perihelion_time.split('.')[0].length+5) }} <b>TDB</b>
                <br>
                <b>Semi major axis</b> : {{ asteroidSelected.orbital_data.semi_major_axis.substring(
                  0, asteroidSelected.orbital_data.semi_major_axis.split('.')[0].length+5) }} <b>au</b>
                <br>
                <br>
                <b>NASA link to asteroid</b> : <a href="{{ asteroidSelected.nasa_jpl_url }}">{{ asteroidSelected.nasa_jpl_url }}</a>
              </p>
              <p *ngIf="asteroidSelected == null">
                Please, choose an asteroid
              </p>
            </div>

        </div>

      </div>
  `,
  styles: [
  ]
})
export class AsteroidDetailsComponent implements OnInit {
  asteroidSelected: NearEarthObject;

  constructor(private loadDataService: LoadDataService) {
    this.loadDataService.asteroidSelected.subscribe( value => {
      this.asteroidSelected = value;

      if(value != null){
          console.log("Asteroid selected : " + this.asteroidSelected.name);
      }
    });
  }

  ngOnInit(): void {

  }
}
