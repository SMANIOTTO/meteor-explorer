import { Component, OnInit, Input } from '@angular/core';
import { AllAsteroidResponse } from '../model/AllAsteroidResponse';
import { JSONParserService } from '../service/json-parser.service';
import { TotalAsteroidResponse } from '../model/TotalAsteroidResponse';

@Component({
  selector: 'astExp-asteroid-all',
  template: `

    <!-- TOTAL ASTEROID INFO -->
    <h3 *ngIf="totalAsteroidResponse != null">
      <b>Total asteroid</b> : {{ totalAsteroidResponse.near_earth_object_count }}
      <br>
      <b>Total close approach count</b> : {{ totalAsteroidResponse.close_approach_count }}
      <br>
      <b>Last update</b> : {{ totalAsteroidResponse.last_updated }}
    </h3>
    <br>
    <br>

    <!-- PAGE NUMBER CHOICE -->
    <form>
      <label for="asteroidNumber">Asteroid per page : </label>
      <br>
      <select name="asteroidNumber" id="asteroidNumber" (change)="reloadPage($event.target.value, numPage)">
        <option value="5">5</option>
        <option value="10">10</option>
        <option value="15">15</option>
        <option value="20">20</option>
      </select>
    </form>
    <br>

    <!-- ASTEROID LIST -->
    <div *ngIf="allAsteroidResponse != null">

      <!-- LIST -->
      <div *ngFor="let in of counter(nbAsteroidPerPage/5); let numRow = index" class="row">
        <div *ngFor="let numAsteroid of [0,1,2,3,4]" class="col">
          <div class="icon-box" style="height:80%">
            <astExp-asteroid-all-display
              [asteroid]="allAsteroidResponse.near_earth_objects[(numRow*5)+numAsteroid]"
            >
            </astExp-asteroid-all-display>
          </div>
        </div>
      </div>

      <!-- PAGE CONTROL -->
      <div class="text-center">
        <h5>
          <a href="#" *ngIf="numPage > 0"
            (click)="reloadPage(nbAsteroidPerPage, -1)">
              <i class="icofont-arrow-left"></i>
          </a>
          {{ numPage }} /
          <t>
            {{ allAsteroidResponse.page.total_pages }}
          </t>
          <a href="#" *ngIf="(allAsteroidResponse == null && numPage == 0) || numPage < allAsteroidResponse.page.total_pages"
              (click)="reloadPage(nbAsteroidPerPage, 1)">
                <i class="icofont-arrow-right"></i>
          </a>
        </h5>
      </div>

    </div>
  `,
  styles: [
  ]
})
export class AsteroidAllComponent implements OnInit {
  nbAsteroidPerPage:      number;
  numPage:                number;
  allAsteroidResponse:    AllAsteroidResponse;
  totalAsteroidResponse:  TotalAsteroidResponse;  //Total asteroid data struct parse from JSON



  constructor(private jsonParser: JSONParserService) { }



  ngOnInit(): void {
    this.nbAsteroidPerPage = 5;
    this.numPage = 0;

    this.reloadPage(this.nbAsteroidPerPage, this.numPage);

    //1) Load total asteroid data
    this.jsonParser.getTotalData().subscribe(
        (data: TotalAsteroidResponse) => {
          console.log("Data from total asteroid get");
          this.loadTotalData(data);
        },

        (err) => { //ERROR getting data
          console.log("Error : " + JSON.stringify(err));
        }
    );
  }



  //RELOAD PAGE//
  //Allows to reload current page with new page settings
  reloadPage(nbAsteroidPerPage: number, numPage: number){
    this.nbAsteroidPerPage = nbAsteroidPerPage;
    this.numPage += numPage;

    //Try to load data from JSON URL
    this.jsonParser.getAllData(this.nbAsteroidPerPage, this.numPage).subscribe(

        //Load data into struct
        (data: AllAsteroidResponse) => {
          this.loadallData(data);
          console.log("all data asteroid get");
        },

        //ERROR getting data
        (err) => { 
          console.log("Error : " + JSON.stringify(err));
        }
    );
  }



  //LOAD all DATA//
  //Allows to load data from observable result for avoiding displaying before data loaded.
  loadallData(data: AllAsteroidResponse){
    //console.log("get : " + data.page.size);

    this.allAsteroidResponse = data;
  }



  //LOAD TOTAL DATA//
  //Allows to load data from observable result for avoiding displaying before data loaded.
  loadTotalData(totalDataGet: TotalAsteroidResponse){
    this.totalAsteroidResponse = totalDataGet;
  }



  //COUNTER//
  //Allows to count until number enter
  counter(value: number){
    return new Array(value);
  }
}
