import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { AsteroidDailyComponent } from './component/asteroid-daily.component';
import { AsteroidDetailsComponent } from './component/asteroid-details.component';
import { AsteroidAllComponent } from './component/asteroid-all.component';
import { AsteroidAllDisplayComponent } from './component/asteroid-all-display.component';

@NgModule({
  declarations: [
    AppComponent,
    AsteroidDailyComponent,
    AsteroidDetailsComponent,
    AsteroidAllComponent,
    AsteroidAllDisplayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
