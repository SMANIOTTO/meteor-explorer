import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsteroidAllComponent } from '../component/asteroid-all.component';

describe('AsteroidAllComponent', () => {
  let component: AsteroidAllComponent;
  let fixture: ComponentFixture<AsteroidAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsteroidAllComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsteroidAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
