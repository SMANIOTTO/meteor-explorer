import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsteroidAllDisplayComponent } from '../component/asteroid-all-display.component';

describe('AsteroidAllDisplayComponent', () => {
  let component: AsteroidAllDisplayComponent;
  let fixture: ComponentFixture<AsteroidAllDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsteroidAllDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsteroidAllDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
