import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsteroidDailyComponent } from '../component/asteroid-daily.component';

describe('AsteroidDailyComponent', () => {
  let component: AsteroidDailyComponent;
  let fixture: ComponentFixture<AsteroidDailyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsteroidDailyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsteroidDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
