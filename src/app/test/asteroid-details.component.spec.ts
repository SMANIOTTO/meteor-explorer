import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsteroidDetailsComponent } from '../component/asteroid-details.component';

describe('AsteroidDetailsComponent', () => {
  let component: AsteroidDetailsComponent;
  let fixture: ComponentFixture<AsteroidDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsteroidDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsteroidDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
