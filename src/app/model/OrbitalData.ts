import { OrbitClass } from './OrbitClass';



/*Allows to get the meteor orbit data*/
export interface OrbitalData{
	aphelion_distance:				    string;		  //Distance between the orbit's farest point to the sun
	ascending_node_longitude:		  string;		  //Longitude of meteor orbit around a body
	data_arc_in_days:				      number;		  //Period between latest and earliest observation
	eccentricity:					        string;		  //Deviation from orbital body
	epoch_osculation:				      string;	  	//Referencial time point
	equinox:						          string;	  	//Time in the year when the sun cross the terrestrial equatorial plane
	first_observation_date:			  string;		  //Date of earliest observation
	inclination:					        string;		  //Angle between a reference plane and the orbital plane
	jupiter_tisserand_invariant:	string;		  //Used to distinguish different kinds of orbits
	last_observation_date:			  string;		  //Date of latest observation
	mean_anomaly:					        string;		  //Period elapsed since the orbiting body passed the hemisphere
	mean_motion:					        string;		  //Angular speed required for the meteor to complete one orbit
	minimum_orbit_intersection:		string;		  //Measure of collision risk with orbital body
	observations_used:				    number;		  //Number of observation for trajectory study
	orbit_determination_date:		  string;		  //Date of orbite determination
	orbit_id:						          string;		  //ID orbit in API
	orbit_uncertainty:				    string;		  //Measure the uncertainty of a perturbed orbital
	orbital_period:					      string;		  //Time when meteor will turn around body orbit
	perihelion_argument:			    string;		  //Angle between ascending node longitude and the orbital center point
	perihelion_distance:			    string;		  //Minimal distance from the sun that the object reaches along its orbit
	perihelion_time:				      string;		  //Time when meteor closest to the sun
	semi_major_axis:				      string;		  //Distance between ellipse center to farest ellipse point
	orbit_class:					        OrbitClass;	//Orbit data of meteor
}
