/*Allows to get page informations from research*/
export interface Page{
	number:         number;
  size:           number;
  total_elements: number;
  total_pages:    number;
}
