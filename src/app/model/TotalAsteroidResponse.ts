/*Allows to get the total NEO which approach near the earth*/
export interface TotalAsteroidResponse {
	near_earth_object_count:	number; //All NEO count
	close_approach_count:		  number;	//All NEO close approach count
	last_updated:				      string;	//Last date that data was update
}
