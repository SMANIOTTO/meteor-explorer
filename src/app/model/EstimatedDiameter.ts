/*Allows to get the size of the meteor*/
export interface EstimatedDiameter{
	estimated_diameter_max:	number;	//Estimation of meteor max size (base on absolute magnitude)
	estimated_diameter_min:	number;	//Estimation of meteor min size (base on absolute magnitude)
}