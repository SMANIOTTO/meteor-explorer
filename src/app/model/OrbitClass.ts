/*Allows to get data from asteroid orbit*/
export class OrbitClass{
	orbit_class_description:	string;	//Similarity with other previous asteroid
	orbit_class_range:			  string;	//Range of the asteroid between semi major axis and perihelion
	orbit_class_type:			    string;	//Orbit disposition (cross body orbit or not)
}
