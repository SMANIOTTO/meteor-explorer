import { NearEarthObject } from './NearEarthObject';
import { Page } from './Page';



/*Allows to get all research asteroid*/
export interface AllAsteroidResponse {
	links:				      object;							    //Links to previous/actual/next JSON
  page:               Page;                   //Page description
  near_earth_objects: Array<NearEarthObject>; //NEO list from research page
}
