import { EstimatedDiameter } from './EstimatedDiameter';



/*Allows to get meteor diameter in different units*/
export interface EstimatedDiameterContainer {
	feet:		    EstimatedDiameter;	//Meteor size in feet
	kilometers:	EstimatedDiameter;	//Meteor size in kilometers
	miles:		  EstimatedDiameter;	//Meteor size in miles
}
