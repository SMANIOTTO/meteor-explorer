import { NearEarthObject } from './NearEarthObject';



/*Allows to get the list of meteor*/
export interface DailyAsteroidResponse {
	links:				      object;							          //Links to previous/actual/next JSON
	element_count:		  number;							          //Total meteor found
	near_earth_objects:	Map<NearEarthObject, string>;	//Meteor list
}
