/*Allows to get the distance from earth to meteor in different units*/
export interface MissDistance {
	astronomical:	string;	//Distance in astronomical length (= 150 billions km)
	kilometers:		string;	//Distance in kilometers
	lunar:			  string;	//Distance in lunar years
	miles:			  string;	//Distance in miles
}
