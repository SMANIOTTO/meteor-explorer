import { MissDistance } from './MissDistance';
import { RelVelocity } from './RelVelocity';



/*Allows to get approach meteor data*/
export interface CloseApproachData{
	close_approach_date_full:	string;			  //Date when meteor will be closest from earth
	orbiting_body:				    string;			  //Body around the meteor orbiting
	miss_distance:				    MissDistance;	//Distance detail from earth to meteor
	relative_velocity:			  RelVelocity;	//Velocity data of the meteor
}
