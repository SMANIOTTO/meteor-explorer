import { CloseApproachData } from './CloseApproachData';
import { EstimatedDiameterContainer } from './EstimatedDiameterContainer';
import { OrbitalData } from './OrbitalData';



/*Allows to get global meteor information*/
export interface NearEarthObject {
	absolute_magnitude_h:				        number;						          //Asteroid brightness
	close_approach_data:				        Array<CloseApproachData>;	  //Proximiy array data
	estimated_diameter:					        EstimatedDiameterContainer;	//Diameter array data
	is_potentially_hazardous_asteroid:	boolean;					          //Dangerous object ?
	is_sentry_object:					          boolean;					          //Impact on earth ?
	name:								                string;						          //Meteor name
	nasa_jpl_url:						            string;						          //Link to the NASA JPL Database website
	neo_reference_id:					          string;						          //API meteor ID
	orbital_data:						            OrbitalData;				        //Orbital array data
}
