/*Allows to get the velocity of the asteroid*/
export interface RelVelocity{
	kilometers_per_hour:	string;	//Speed in km/h
	miles_per_hour:			  string;	//Speed in mph
}
