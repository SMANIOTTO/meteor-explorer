import { Component } from '@angular/core';

@Component({
  selector: 'astExp-root',
  template: `
    <!------- HEADER ------->
    <header id="header" class="header-tops">
      <div class="container">

        <!-- TITLE -->
        <h1>ASTEROID EXPLORER</h1>
        <h2>A web application for exploring asteroid fell near our planet</h2>

        <!-- NAV BAR -->
        <nav class="nav-menu d-none d-lg-block">
          <ul>
            <li class="active"><a href="#header">Home</a></li>
            <li><a href="#daily">Daily</a></li>
            <li><a href="#all">All</a></li>
          </ul>
        </nav>
        <br>

        <!-- CREDIT -->
        <h6>Asteroid fallen by <a href="https://pixabay.com/fr/illustrations/meteor-ast%C3%A9ro%C3%AFde-espace-catastrophe-3129573/">AlexAntropov86 / Pixabay</a></h6>

      </div>
    </header>



    <!-------  DAILY  ------->
    <section id="daily" class="daily">

      <!-- CONTENT -->
      <div class="daily-me container">

        <!-- TITLE -->
        <div class="section-title">
          <h2>Daily</h2>
          <p>Daily asteroid near earth</p>
        </div>

        <!-- METEOR DETAILS -->
        <div class="row">

          <!-- PICTURE -->
          <div class="col-lg-4" data-aos="fade-right">
            <img src="assets/img/asteroid_impact.jpg" class="img-fluid" alt="">
            <h6>Asteroid impact by <a href="https://pixabay.com/fr/photos/meteor-crat%C3%A8re-impact-de-m%C3%A9t%C3%A9orite-67495/">WikiImages / Pixabay</a></h6>
          </div>

          <!-- ASTEROID LIST -->
          <div class="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">
            <astExp-asteroid-daily></astExp-asteroid-daily>
          </div>
        </div>
      </div>

      <!-- ASTEROID DATA DISPLAY -->
      <astExp-asteroid-details></astExp-asteroid-details>

    </section>



    <!------- ALL ------->
    <section id="all" class="all">
      <div class="container">

        <!-- ASTEROID all -->
        <div class="section-title">
          <h2>All</h2>
          <p>All asteroid discovered</p>
          <astExp-asteroid-all></astExp-asteroid-all>
        </div>

        <!-- CREDIT -->
        <h6>Sun by <a href="https://pixabay.com/fr/illustrations/sun-plan%C3%A8te-le-syst%C3%A8me-solaire-1506019/">Valera268268 / Pixabay</a></h6>
        <h6>Earth by <a href="https://pixabay.com/fr/photos/terre-plan%C3%A8te-bleue-monde-plan%C3%A8te-11015/">WikiImages / Pixabay</a></h6>
      </div>
    </section>

    
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/personal-free-resume-bootstrap-template/ -->
      Created by <a href="https://www.linkedin.com/in/gabriel-smaniotto-80354715a/">SMANIOTTO Gabriel</a>
      <br>
      & Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
    <script src="assets/vendor/counterup/counterup.min.js"></script>
    <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/venobox/venobox.min.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>
  `,
  styles: []
})
export class AppComponent {

  

}
